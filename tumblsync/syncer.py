# -*- encoding: utf-8 -*-
from __future__ import (
    unicode_literals, absolute_import, print_function, division)
import argparse
import binascii
import shelve
import socket
import re
import feedparser
import requests
from time import sleep
from urlparse import urlparse, urlunparse
from HTMLParser import HTMLParser
from tumblpy import Tumblpy, TumblpyAuthError
from .logger import logger
from . import settings

socket.setdefaulttimeout(90)
unescape = HTMLParser().unescape


class Post(object):

    def __init__(self, entry, tumblr_id=None, default_tags=()):
        self.entry = entry
        self.tumblr_id = tumblr_id
        self.default_tags = list(default_tags)

    @property
    def tags(self):
        return self.default_tags + \
            [tag['term'] for tag in self.entry.get('tags', [])]

    @property
    def embed(self):
        for link in self.entry['links']:
            if link['rel'] == 'enclosure' and link['type'].startswith('text/'):
                return link['href']

    @property
    def params(self):
        params = {
            'slug': filter(bool, self.entry['link'].split('/'))[-1],
            'source_url': self.entry['link'],
            'tags': ','.join(self.tags)}
        if self.tumblr_id:
            params['id'] = self.tumblr_id
        iframe_src = self.embed
        if iframe_src:  # This condition must never change for updates
            params.update({
                'type': 'video',
                'embed': (
                    '<iframe width="700" height="394" src="{src}"'
                    ' frameborder="0" allowfullscreen></iframe>')
                        .format(src=iframe_src),
                'caption': '<h2 itemprop="name">{title}</h2>{body}'.format(
                    title=unescape(self.entry['title']),
                    body=self.entry['content'][0]['value'])})
        else:
            params.update({
                'type': 'text',
                'title': unescape(self.entry['title']),
                'body': self.entry['content'][0]['value']})
        return params


class Syncer(object):

    DB_TEMPLATE = 'database-{hash}.pickle'

    def __init__(self, feed, blog, default_tags=(), link_pattern='.*'):
        self.feed_url = feed
        self.blog = blog
        self.default_tags = list(default_tags)
        self.link_pattern = re.compile(link_pattern)
        database_hash = binascii.crc32(self.feed_url + self.blog) & 0xffffffff
        self.database_name = self.DB_TEMPLATE.format(hash=database_hash)
        self.database = shelve.open(self.database_name, writeback=True)
        self.tumblr = Tumblpy(settings.CONSUMER_KEY, settings.CONSUMER_SECRET,
                              settings.OAUTH_TOKEN, settings.OAUTH_SECRET)

    def sync(self):
        # Cache busting
        url_parsed = urlparse(self.feed_url)
        if not url_parsed.query:
            feed_url = urlunparse(url_parsed[:4] + ('_', ''))
        else:
            feed_url = self.feed_url
        # Feedparser isn’t good at HTTP
        response = requests.get(feed_url, timeout=10)
        response.raise_for_status()
        # And neither at Unicode
        feed = feedparser.parse(response.content)
        try:
            self.tumblr.get('followers', self.blog)
        except TumblpyAuthError:
            # The API always raises 401 Not Authorized on errors.
            # This should always work on our own Tumblrs and helps
            # distinguish authorization errors from other errors.
            logger.error('Error connecting to %s', self.blog, exc_info=True)
            return
        for entry in reversed(feed.entries):
            entry_id = entry['id'].encode('utf-8')
            if entry_id in self.database:
                tumblr_id = self.database[entry_id]['tumblr_id']
                post = Post(entry, tumblr_id, self.default_tags)
            else:
                post = Post(entry, default_tags=self.default_tags)
            if set(settings.SKIP_TAGS) & set(post.tags):
                logger.info('Skipping due to tag: %s', entry['link'])
                continue
            if not self.link_pattern.match(entry['link']):
                logger.info('Skipping due to URL mismatch: %s', entry['link'])
                continue
            if self.database.get(entry_id, {}).get('error'):
                logger.warn('Skipping due to error record: %s', entry['link'])
                continue
            if not post.tumblr_id:
                new_post = self.tumblr.post(
                    'post', self.blog, params=post.params)
                self.database[entry_id] = {'tumblr_id': new_post['id'],
                                           'updated': entry['updated_parsed']}
                logger.info('Created new post %s from %s',
                            new_post['id'], entry['link'])
            elif self.database[entry_id]['updated'] < entry['updated_parsed']:
                try:
                    self.tumblr.post(
                        'post/edit', self.blog, params=post.params)
                except TumblpyAuthError:  # 401 Not Authorized includes deleted
                    self.database[entry_id]['error'] = True
                    logger.error('Error-marked record for post %s from %s:',
                                 post.tumblr_id, entry['link'], exc_info=True)
                else:
                    self.database[entry_id]['updated'] = entry['updated_parsed']
                    logger.info('Updated post %s from %s',
                                post.tumblr_id, entry['link'])
        self.database.sync()


def run():
    parser = argparse.ArgumentParser(description='Syncs Tumblr.')
    parser.add_argument('-f', '--feed', required=True,
                        help='URL of the Atom feed')
    parser.add_argument('-b', '--blog', required=True,
                        help='Domain of the blog')
    parser.add_argument('-t', '--tags', default='',
                        help='Comma-separated default tags to be prepended')
    parser.add_argument('-p', '--pattern', default='.*',
                        help='Regex filter for entry URLs')
    args = parser.parse_args()
    syncer = Syncer(args.feed, args.blog,
                    args.tags.split(','), args.pattern)
    syncer.sync()
