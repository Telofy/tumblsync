#!/usr/bin/env python
from setuptools import setup

setup(
    name='tumblsync',
    version='0.1.0',
    author='Telofy',
    author_email='tumblsync@yu-shin.de',
    include_package_data=True,
    extras_require=dict(
        test=['coverage>=3.4'],
    ),
    install_requires=[
        'requests',
        'python-tumblpy>=1.0.3',
        'feedparser>=5.1.3',
    ],
    zip_safe=False,
    entry_points={
        'console_scripts': [
            'tumblsync = tumblsync.syncer:run'
        ]
    }
)
