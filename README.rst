=========
Tumblsync
=========

A simple tool to keep Tumblr in sync with a feed by posting new posts on Tumblr and updating them when they change on the feed.

You can define your OAuth credentials etc. in tumblsync/settings_override.py (which does not exist).

This script is rather specialized for our own Wordpress Atom feeds. RSS feeds often don’t contain GUIDs (they’re still optional even in RSS 2.0), in which case they won’t work, but it has a few more requirements on Atom feeds too.

You can enter it into your crontab to run it regularly.

- Feeds need to contain the full content in a content element.
- Separated by slashes, the last nonempty element in a URL need to be the slug.

Example::

    bin/tumblsync \
        --feed http://woy.derpynews.com/feed/atom/ \
        --blog wandernews.tumblr.com \
        --tags "Wander Over Yonder,WOY,Lord Hater,Sylvia,Disney"
